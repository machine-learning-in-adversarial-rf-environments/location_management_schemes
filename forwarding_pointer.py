from node import Node
from queue import Queue

class ForwardingPointer(Node):

    def __init__(self, node_id):
        super().__init__(node_id)
        self.forwarding_location = None

    def set_forwarding_location(self, location):
        self.forwarding_location = location

    def get_forwarding_location(self):
        if self.forwarding_location is not None:
            return self.forwarding_location
        else:
            return None


class ForwardingFactory:

    def __init__(self,node):
        self.pointer_nodes = []
        self.node = node


    def __str__(self):
        return self.node

    def purge_pointer_nodes(self):
        if len(self.pointer_nodes) > 0:
            self.node.set_forwarding_location(self.pointer_nodes[len(self.pointer_nodes) - 1])

    def forwarding_location_path(self):
        forwarding_location = self.node.get_forwarding_location()
        while forwarding_location is not None:
            print(forwarding_location.node_id, '-> ', end='')
            self.pointer_nodes.append(forwarding_location)
            forwarding_location = forwarding_location.get_forwarding_location()
        print()


def UI():
    print("-------------------")
    # create nodes
    nodes = []
    root = ForwardingPointer('root')
    nodes.append(root)

    print("Enter number of nodes: ", end="")
    number_of_nodes = int(input())

    nodes_children = []
    while (1):
        print("Enter node with child: ", end="")
        node_child = str(input())
        if node_child == "0":
            break
        nodes_children.append(node_child)


    for i in range(1,number_of_nodes):
        nodes.append(ForwardingPointer(str(i)))

    for i in nodes_children:
        arr = i.split(',')
        if arr[0]=="root":
            node = 0
        else:
            node = int(arr[0])
        child = int(arr[1])
        nodes[node].add_child(nodes[child])

    visited = []

    gTree_queue = Queue()

    visited =root.breadth_first_traversal(gTree_queue, visited)

    print('breadthFirstTraversal')
    for i in visited:
        print(i)
    print("-------------------")
    print("setup pointer nodes")
    print("-------------------")
    forwarding_nodes = []
    while (1):
        print("Enter node with forwarding location: ", end="")
        node_forwarder = str(input())
        if node_forwarder == "0":
            break
        forwarding_nodes.append(node_forwarder)

    for i in forwarding_nodes:
        print(i)
        arr = i.split(',')
        node_index = int(arr[0])
        node = nodes[node_index]
        forwarding_location_index = int(arr[1])
        forwarding_location = nodes[forwarding_location_index]
        node.set_forwarding_location(forwarding_location)
        nodes[node_index] = node

    for node in nodes:
        print(node.node_id, '-> ', end='')
        forwarding_factory = ForwardingFactory(node)
        forwarding_factory.forwarding_location_path()

    visited.clear()
    gTree_queue = Queue()
    visited =root.breadth_first_traversal(gTree_queue, visited)

    print('breadthFirstTraversal before purge')
    for i in visited:
        loop_factory = ForwardingFactory(i)
        print(i.node_id, '-> ', end='')
        loop_factory.forwarding_location_path()
    print("-------------------")

    print('breadthFirstTraversal after purge')
    for node in nodes:
        print(node.node_id)
        loop_factory = ForwardingFactory(node)
        loop_factory.purge_pointer_nodes()
        try:
            forwarding_location_id = loop_factory.node.get_forwarding_location().node_id
        except AttributeError:
            forwarding_location_id = None
        print(loop_factory.node.node_id, "->", forwarding_location_id)

    visited.clear()
    gTree_queue = Queue()
    visited =root.breadth_first_traversal(gTree_queue, visited)
    print('breadthFirstTraversal')
    for i in visited:
        loop_factory = ForwardingFactory(i)
        print(i.node_id, '-> ', end='')
        loop_factory.forwarding_location_path()
    print("-------------------")

if __name__ == "__main__":
    UI()

