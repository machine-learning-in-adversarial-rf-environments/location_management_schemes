# https://python-forum.io/Thread-general-tree-traversal

class Node:

    def __init__(self, node_id):
        self.node_children = []
        self.node_id = node_id

    def __str__(self):
        return self.node_id

    def add_child(self, child):
        self.node_children.append(child)

    def preorder_traversal(self, preorder_list):
        preorder_list.append(self)
        for i in self.node_children:
            i.preorder_traversal(preorder_list)
        return preorder_list

    def breadth_first_traversal(self, node_queue, visited):
        visited.append(self)
        for i in self.node_children:
            node_queue.put(i)

        while node_queue.qsize() != 0:
            return node_queue.get().breadth_first_traversal(node_queue, visited)
        return visited


