from node import Node

class WorkingSet(Node):
    pass

#
# import array as arr
# import time
# import os
#
#
# def main():
#     # create 4 base stations
#     station1 = arr.array('i', [30, 0, 20])
#     station2 = arr.array('i', [60, 0, 20])
#     station3 = arr.array('i', [90, 0, 20])
#     station4 = arr.array('i', [120, 0, 20])
#
#     print('There are 4 cell towers with the power of 20. This leaves overlap between two distinct towers.')
#     xStart = int(input('Enter the starting X coordinate for your phone: '))
#     xMax = 160
#     y = 0
#
#     xLast = 0;
#
#     for x in range(xStart, xMax):
#         print('Mobile device is located at ', x)
#
#         distance1 = abs(x - station1[0])
#         distance2 = abs(x - station2[0])
#         distance3 = abs(x - station3[0])
#         distance4 = abs(x - station4[0])
#
#         numTowers = 0
#         handshake = 0
#         pad = 30
#         nodePad = 45
#         wsPad = 70
#
#         print(('.') * (wsPad), 'Base', '.' * (wsPad), '.' * (20))
#         print(('.') * (nodePad), 'WS', '.' * (nodePad), 'WS', '.' * (nodePad + 20))
#         print('.' * pad, 'T1', '.' * pad, 'T2', '.' * pad, 'T3', '.' * pad, 'T4', '.' * pad)
#         print(' ' * x, '*')
#
#         if (distance1 <= station1[2]):
#             print('Cell is close to base station 1.')
#             print('Cell is untilizing work station 1.')
#             print('Cell is untilizing base 1.')
#             numTowers += 1
#         if (distance2 <= station2[2]):
#             print('Cell is close to base station 2.')
#             print('Cell is untilizing work station 1.')
#             print('Cell is untilizing base 1.')
#             numTowers += 1
#         if (distance3 <= station3[2]):
#             print('Cell is close to base station 3.')
#             print('Cell is untilizing work station 2.')
#             print('Cell is untilizing base 1.')
#             numTowers += 1
#         if (distance4 <= station4[2]):
#             print('Cell is close to base station 4.')
#             print('Cell is untilizing work station 2.')
#             print('Cell is untilizing base 1.')
#             numTowers += 1
#
#         print('There are {} towers close to your device.'.format(numTowers))
#
#         if (numTowers > 1):
#             print('Handshake between towers has started.')
#             handshake = 1
#         if (numTowers <= 1 & handshake > 0):
#             print('Handshake has completed.')
#             handshake = 0
#
#         time.sleep(.5)
#         os.system('cls')
#
#
# if __name__ == '__main__':
#     main()
#
